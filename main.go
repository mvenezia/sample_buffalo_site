package main

import (
	"log"

	"gitlab.com/mvenezia/sample_buffalo_site/actions"
)

func main() {
	app := actions.App()
	if err := app.Serve(); err != nil {
		log.Fatal(err)
	}
}
