package grifts

import (
	"github.com/gobuffalo/buffalo"
	"gitlab.com/mvenezia/sample_buffalo_site/actions"
)

func init() {
	buffalo.Grifts(actions.App())
}
